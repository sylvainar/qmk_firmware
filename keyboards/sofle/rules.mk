# MCU name
MCU = atmega32u4

# Bootloader selection
BOOTLOADER = caterina

SPLIT_KEYBOARD = yes
DEFAULT_FOLDER = sofle/rev1
SWAP_HANDS_ENABLE = yes
EXTRAKEY_ENABLE = yes
MOUSEKEY_ENABLE = yes
WPM_ENABLE = yes
DYNAMIC_MACRO_ENABLE = yes
